import { browser, by, element } from 'protractor';

export class AppPage {
  navigateTo() {
    return browser.get(browser.baseUrl) as Promise<any>;
  }

  async getFromCurrencyInput() {
    return (await element(by.name('from-currency-input'))).getText();
  }

  async getToCurrencyInput() {
    return (await element(by.name('to-currency-input'))).getText();
  }
}
