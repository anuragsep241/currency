import {
    HttpClientTestingModule,
    HttpTestingController
} from '@angular/common/http/testing';
import { getTestBed, TestBed } from '@angular/core/testing';
import { CurrencyService } from './currency.service';
import { Currency, GraphPoint } from './currency.model';


describe('CurrencyService', () => {
  let injector;
  let service: CurrencyService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [CurrencyService]
    });

    injector = getTestBed();
    service = injector.get(CurrencyService);
    httpMock = injector.get(HttpTestingController);
  });

  describe('#getCurrencies', () => {
    it('should return an Observable<Currency[]>', () => {
      const mockCurrencies: Currency[] = [
        { name: 'USD', value: 1 },
        { name: 'EUR', value: 1.2 },
      ];

      service.getCurrencies().subscribe(currencies => {
        expect(currencies.length).toBe(2);
        expect(currencies).toEqual(mockCurrencies);
      });
    });
  });

  describe('#getCurrencyTrend', () => {
    it('should return an Observable<GraphPoint[]>', () => {
      const mockTrends: GraphPoint[] = [
       {date: new Date(), value: 1}
      ];

      service.getCurrencyTrend('USD', 'EUR').subscribe(currencies => {
        expect(currencies.length).toBe(2);
        expect(currencies).toEqual(mockTrends);
      });
    });
  });
});
