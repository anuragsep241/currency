import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Currency, GraphPoint } from './currency.model';

@Injectable({
  providedIn: 'root',
})
export class CurrencyService {
  constructor(private http: HttpClient) {}

  /**
   * Get all the latest currencies
   */
  getCurrencies(): Observable<Array<Currency>> {
    return this.http.get<any>(`https://api.exchangeratesapi.io/latest`).pipe(
      map((data) => {
        data.rates[data.base] = 1;
        return Object.keys(data.rates).reduce((comm, curr) => {
          comm.push({ name: curr, value: data.rates[curr] });
          return comm;
        }, []);
      })
    );
  }

  /**
   * Get the historical data to draw a graph
   * @param sourceCurrency cource currency
   * @param targetCurrency target currency
   */
  getCurrencyTrend(
    sourceCurrency: string,
    targetCurrency: string
  ): Observable<Array<GraphPoint>> {
    const date = new Date();
    date.setMonth(date.getMonth() - 1);
    const startDate = date.toISOString().slice(0, 10);
    const currentDate = new Date().toISOString().slice(0, 10);
    const url = `https://api.exchangeratesapi.io/history?start_at=` +
     `${startDate}&end_at=${currentDate}&symbols=${sourceCurrency},${targetCurrency}`;
    return this.http.get<any>(url).pipe(
      map((data) => {
        return Object.keys(data.rates).reduce((comm, curr) => {
          comm.push({
            date: new Date(curr),
            value:
              data.rates[curr][sourceCurrency] /
              data.rates[curr][targetCurrency],
          });
          return comm;
        }, []);
      })
    );
  }
}
