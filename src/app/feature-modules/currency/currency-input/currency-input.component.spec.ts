import { HttpClient } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed, async, getTestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { of } from 'rxjs/internal/observable/of';
import { CurrencyModule } from '../currency.module';
import { CurrencyInputComponent } from './currency-input.component';


describe('CurrencyInputComponent', () => {
  let fixture;
  let component;
  beforeEach(async(() => {

    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, FormsModule],
      declarations: [CurrencyInputComponent],
      providers: [
      ]
    })
      .compileComponents();

    fixture = TestBed.createComponent(CurrencyInputComponent);
    component = fixture.debugElement.componentInstance;
  }));

  it('should create the component', () => {
    expect(component).toBeTruthy();
  });

  it('should emit on amount change', () => {
    component.currencyList = [{ name: 'USD', value: 1 }];
    component.ngOnInit();
    spyOn(component.amountUpdate, 'emit');
    component.onAmountUpdate();
    expect(component.amountUpdate.emit).toHaveBeenCalled();
  });

  it('should emit on currency change', () => {
    component.currencyList = [{ name: 'USD', value: 1 }];
    component.ngOnInit();
    spyOn(component.currencyChange , 'emit');
    component.onCurrencyChange();
    expect(component.currencyChange.emit).toHaveBeenCalled();
  });

});
