import { AppPage } from './app.po';
import { browser, logging } from 'protractor';

describe('workspace-project App', () => {
  let page: AppPage;

  beforeEach(() => {
    page = new AppPage();
  });

  it('should display 1000 value in from Cuurency input', () => {
    page.navigateTo();
    expect(page.getFromCurrencyInput()).toEqual('1000');
  });
  it('should display 1000 value in to Cuurency input', () => {
    page.navigateTo();
    expect(page.getToCurrencyInput()).toEqual('1000');
  });

  afterEach(async () => {
    // Assert that there are no errors emitted from the browser
    const logs = await browser.manage().logs().get(logging.Type.BROWSER);
    expect(logs).not.toContain(jasmine.objectContaining({
      level: logging.Level.SEVERE,
    } as logging.Entry));
  });
});
