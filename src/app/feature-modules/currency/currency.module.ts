import { BrowserModule } from '@angular/platform-browser';
import { CurrencyPipe} from '@angular/common';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

import { CurrencyConversionComponent } from './currency-conversion/currency-conversion.component';
import { CurrencyInputComponent } from './currency-input/currency-input.component';

@NgModule({
  declarations: [
    CurrencyConversionComponent,
    CurrencyInputComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [CurrencyPipe],
  exports: [
    CurrencyConversionComponent,
    CurrencyInputComponent,
  ]
})
export class CurrencyModule { }
