export interface Currency {
    name: string;
    value: number;
}

export interface GraphPoint {
 date: Date;
 value: number;
}
