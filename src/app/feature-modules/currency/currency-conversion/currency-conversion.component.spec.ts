import { HttpClient } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed, async, getTestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { of } from 'rxjs/internal/observable/of';
import { CurrencyInputComponent } from '../currency-input/currency-input.component';
import { Currency } from '../currency.model';
import { CurrencyService } from '../currency.service';
import { CurrencyConversionComponent } from './currency-conversion.component';

const mockCurrencies: Currency[] = [
  { name: 'USD', value: 1 },
  { name: 'EUR', value: 1.2 },
];

class MockcurrencySerivce {
  getCurrencies() {
    return of(mockCurrencies);
  }
}
describe('CurrencyConversionComponent', () => {
  let fixture;
  let component;

  beforeEach(async(() => {
    let injector;
    let service: CurrencyService;
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, FormsModule],
      declarations: [CurrencyConversionComponent, CurrencyInputComponent],
      providers: [{ provide: CurrencyService, useClass: MockcurrencySerivce }],
    }).compileComponents();
    injector = getTestBed();
    service = injector.get(CurrencyService);
    fixture = TestBed.createComponent(CurrencyConversionComponent);
    component = fixture.debugElement.componentInstance;
  }));

  it('should create the component', () => {
    expect(component).toBeTruthy();
  });

  it('should call the service api', () => {
    component.ngOnInit();
    expect(component.currencyList.length).toBe(2);
  });

  it('should convert amount in target when source is changed', () => {
    component.ngOnInit();
    expect(component.currencyList.length).toBe(2);
    component.onAmountUpdate('from', 100);
    expect(component.toAmount).toBe(100);
  });

  it('should convert amount in source when target is changed', () => {
    component.ngOnInit();
    expect(component.currencyList.length).toBe(2);
    component.onAmountUpdate('to', 100);
    expect(component.fromAmount).toBe(100);
  });

  it('should convert amount in target when source currency is changed', () => {
    component.ngOnInit();
    expect(component.currencyList.length).toBe(2);
    component.onCurrencyUpdate('to', { name: 'USD', value: 1 });
    expect(component.fromAmount).toBe(1000);
  });

  it('should convert amount in surce when target currency is changed', () => {
    component.ngOnInit();
    expect(component.currencyList.length).toBe(2);
    component.onCurrencyUpdate('from', { name: 'USD', value: 1 });
    expect(component.toAmount).toBe(1000);
  });
});
