import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Currency } from '../currency.model';

@Component({
  selector: 'app-currency-input',
  templateUrl: './currency-input.component.html',
  styleUrls: ['./currency-input.component.scss'],
})
export class CurrencyInputComponent implements OnInit {
  selectedCurrency: Currency;
  @Input()amount: number;
  @Input() currencyList: Array<Currency>;
  @Output() amountUpdate = new EventEmitter<number>();
  @Output() currencyChange = new EventEmitter<Currency>();
  constructor() {}

  onAmountUpdate() {
    this.amountUpdate.emit(this.amount);
  }

  onCurrencyChange() {
    this.currencyChange.emit(this.selectedCurrency);
  }

  ngOnInit() {
    if (this.currencyList.length > 0 ) {
      this.selectedCurrency = this.currencyList[0];
    }
  }
}
