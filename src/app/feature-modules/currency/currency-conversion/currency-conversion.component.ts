import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { Currency } from '../currency.model';
import { CurrencyService } from '../currency.service';

@Component({
  selector: 'app-currency-conversion',
  templateUrl: './currency-conversion.component.html',
  styleUrls: ['./currency-conversion.component.scss'],
})
export class CurrencyConversionComponent implements OnInit, OnDestroy {
  toAmount = 1000;
  fromAmount = 1000;
  toCurrency: Currency;
  fromCurrency: Currency;
  currencyList: Array<Currency> = [];
  curencySubcription: Subscription;

  constructor(private currencyService: CurrencyService) { }
  /**
   * on amount change
   * @param source source of change
   * @param amount new amount
   */
  onAmountUpdate(source: string, amount: number) {
    if (source === 'to') {
      this.toAmount = amount;
      this.fromAmount = this.convertCurrency(
        this.toCurrency,
        this.fromCurrency,
        this.toAmount
      );
    } else {
      this.fromAmount = amount;
      this.toAmount = this.convertCurrency(
        this.fromCurrency,
        this.toCurrency,
        this.fromAmount
      );
    }
  }

  /**
   * On currency change
   * @param source source of event
   * @param currency changing currecny
   */
  onCurrencyUpdate(source: string, currency: Currency) {
    if (source === 'to') {
      this.toCurrency = currency;
      this.fromAmount = this.convertCurrency(
        this.toCurrency,
        this.fromCurrency,
        this.toAmount
      );
    } else {
      this.fromCurrency = currency;
      this.toAmount = this.convertCurrency(
        this.fromCurrency,
        this.toCurrency,
        this.fromAmount
      );
    }
  }

  /**
   * Converts the source amount by using  source currency  to target currency
   * @param sourceCurrency source curency
   * @param targetCurrency target currency
   * @param sourceAmount changed amount
   */
  convertCurrency(
    sourceCurrency: Currency,
    targetCurrency: Currency,
    sourceAmount: number
  ) {
    if (sourceAmount) {
      return (targetCurrency.value / sourceCurrency.value) * sourceAmount;
    }
  }

  ngOnInit() {
    this.curencySubcription = this.currencyService.getCurrencies().subscribe((data) => {
      this.currencyList = data;
      if (this.currencyList.length > 0) {
        this.toCurrency = this.currencyList[0];
        this.fromCurrency = this.currencyList[0];
      }
    });
  }

  ngOnDestroy() {
    if (this.curencySubcription) {
      this.curencySubcription.unsubscribe();
    }
  }
}
