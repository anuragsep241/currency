# Currency

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.0.0.

## Setup
Run `npm install` in project folder terminal
## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `npm run test` to execute the unit tests. 100% coverge.

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests. Please update chrome browser to latest if e2e test are  giving error.



## Functionality Coverred
* Ability to select the source and target currencies
* Ability to input the source amount
* Conversion rates must be pulled from a third-party API.

* Ability to perform multiple conversions at the same time
* Bidirectional conversion (user can input either source or target amount)
* Show historical rates evolution (e.g. with chart) 1/2
  -- Created service to get Graph Data, but did not create component to useit. Approach was to create a dummy d3.js enabled component to draw the graph.

