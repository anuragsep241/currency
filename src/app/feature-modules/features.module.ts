import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { CurrencyModule } from './currency/currency.module';

@NgModule({
  declarations: [],
  imports: [
    BrowserModule,
    CurrencyModule
  ],
  providers: [],
  exports: [CurrencyModule]
})
export class FeaturesModule { }
